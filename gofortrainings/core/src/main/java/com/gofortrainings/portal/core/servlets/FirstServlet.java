package com.gofortrainings.portal.core.servlets;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

@Component(
		service=Servlet.class,
		property = {
				"sling.servlet.resourceTypes=gofortrainings/components/structure/page",
				"sling.servlet.methods=POST",
				"sling.servlet.selectors=articles",
				"sling.servlet.extension=json",
				"sling.servlet.methods=GET"
				}
		)
public class FirstServlet extends SlingAllMethodsServlet{

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		
		String articlePagePath = "/content/gofortrainings/en_US/index/articles";
		
		ResourceResolver resolver = request.getResourceResolver();
		PageManager pageManager = resolver.adaptTo(PageManager.class);
		Page articlePage = pageManager.getPage(articlePagePath);
		
		JSONArray pageList = new JSONArray();
		
		if(articlePage != null) {
			Iterator<Page> childPages = articlePage.listChildren();
			while (childPages.hasNext()) {
				Page page = (Page) childPages.next();
				
				JSONObject pageObj = new JSONObject();
				try {
					pageObj.put("title", page.getTitle());
					pageObj.put("description", page.getDescription());
					pageObj.put("path", page.getPath()+".html");
					pageObj.put("createdDate", page.getLastModified().getTime());
					
					Resource imageResource = resolver.getResource(page.getPath()+"/jcr:content/image");	
					pageObj.put("thumbnail", imageResource.getValueMap().get("fileReference",String.class));
					pageList.put(pageObj);			
					
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}			
				
			}
		}
		
		response.setContentType("application/json");
		try {
			JSONObject result = new JSONObject();
			result.put("result", pageList);
			response.getWriter().print(result.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("application/json");
		JSONObject obj = new JSONObject();
		try {
			obj.put("title", "data from resource based servlet");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.getWriter().print(obj.toString());
		
	}
	
}
