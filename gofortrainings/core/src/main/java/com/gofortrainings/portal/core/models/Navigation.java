package com.gofortrainings.portal.core.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables=Resource.class)
public class Navigation {
	
	@ValueMapValue
	private String skipNavigationRoot;
	
	@ChildResource
	private Resource navdata; ///conf/gofortrainings/settings/wcm/templates/home-page/structure/jcr:content/root/navigation/navdata
	
	
	@ChildResource
	private Resource navdata1;
	
	List<NavigationItem> navList;
	
	@PostConstruct
	public void init() {
		
		navList = new ArrayList<>();
				
		if(navdata!=null) {
			
			Iterator<Resource>  navlist = navdata.listChildren();
			while (navlist.hasNext()) {
				Resource resource = (Resource) navlist.next(); // /conf/gofortrainings/settings/wcm/templates/home-page/structure/jcr:content/root/navigation/navdata/item0
				
				NavigationItem item = resource.adaptTo(NavigationItem.class);
				navList.add(item);
				
			}
		}
	}

	public List<NavigationItem> getNavList() {
		return navList;
	}

	
}
