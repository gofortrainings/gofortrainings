package com.gofortrainings.portal.core.services.impl;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gofortrainings.portal.core.services.FirstService;

@Component
public class SecondComponent {
	
	public static final Logger log = LoggerFactory.getLogger(SecondComponent.class);
	
	@Reference
	public FirstService firstService;
	
	@Activate
	public void activate() {
		log.info("Activate methos is being executed");
		log.info("Get Data from FirstComponent.java {}",firstService.getData());
	}
	
	@Deactivate
	public void deactivate() {
		log.info("Deactivate methos is being executed");
	}
	
	@Modified
	public void update() {
		log.info("Update methos is being executed");
	}

}
