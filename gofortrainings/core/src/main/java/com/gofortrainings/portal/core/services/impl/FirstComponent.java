package com.gofortrainings.portal.core.services.impl;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.ServiceScope;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gofortrainings.portal.core.services.FirstService;

@Component(service=FirstService.class,immediate=true)
@Designate(ocd=Configuration.class)
public class FirstComponent implements FirstService {
	
	public static final Logger log = LoggerFactory.getLogger(FirstComponent.class);
	

	@Override
	public String getData() {		
		return "First Name"+config.firstname()+" :: last name - "+config.lastname()+" :: Gender - "+config.gender();
	}
	
	@Activate
	Configuration config;
		
	
	@Deactivate
	public void deactivate() {
		
		log.info("Deactivate methos is being executed");
	}
	
	@Modified
	public void update() {
		log.info("First Name : {}",config.firstname());
		log.info("Last Name : {}",config.lastname());
		log.info("Gender Name :{}",config.gender());
		log.info("Update methos is being executed");
	}

	
}
