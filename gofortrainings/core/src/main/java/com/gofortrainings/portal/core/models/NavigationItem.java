package com.gofortrainings.portal.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables=Resource.class)
public class NavigationItem {

	@ValueMapValue 
	private String navlabel;
	
	@ValueMapValue 
	private String navpath;
	
	@ValueMapValue 
	private String alttext;

	public String getNavlabel() {
		return navlabel;
	}

	public String getNavpath() {
		return navpath;
	}

	public String getAlttext() {
		return alttext;
	}
	
}
