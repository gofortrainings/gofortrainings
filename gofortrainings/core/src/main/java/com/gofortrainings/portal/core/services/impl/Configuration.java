package com.gofortrainings.portal.core.services.impl;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name="First Component Configuration")
public @interface Configuration {
	
	@AttributeDefinition(description="Configure First Name",name="First Name",required=true,type=AttributeType.STRING)
	public String firstname();
	
	@AttributeDefinition(description="Configure Last Name",name="Last Name",required=true,type=AttributeType.STRING)
	public String lastname();
	
	@AttributeDefinition(description="Configure Gender",name="Gender",required=true,type=AttributeType.STRING)
	public String gender() default "male";
	
}
