package com.gofortrainings.portal.core.listeners;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(
			service=EventHandler.class,
			property = {
	            EventConstants.EVENT_TOPIC + "=org/apache/sling/api/resource/Resource/ADDED",
	            EventConstants.EVENT_FILTER+"=(path=/content/gofortrainings/en_US/index/articles/*)"
			}		
		)
public class UpdateArticleTags implements EventHandler{

	public static final Logger log = LoggerFactory.getLogger(UpdateArticleTags.class);
	
	
	@Reference
	private ResourceResolverFactory factory;
	
	@Override
	public void handleEvent(Event event) {
		
		Map<String,Object> props = new HashMap<>();
		props.put(factory.SUBSERVICE, "writeservice");
		
		ResourceResolver resolver = null;
		try {
			resolver = factory.getServiceResourceResolver(props);
			
			String path = event.getProperty("path").toString();
			log.info("path :: {}",path);
			
			if(path !=null && path.endsWith("/jcr:content")) {
				//path = /content/gofortrainings/en_US/index/articles/article-9/jcr:content
				
				Resource resource = resolver.getResource(path);
				if(resource!=null) {
					Node node = resource.adaptTo(Node.class);
					node.setProperty("cq:tags", "we-retail:activity/hiking");					
					node.getSession().save();					
					//resolver.adaptTo(Session.class).save();
				}
			}
			
		} catch (LoginException | RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			
		
	}

}
