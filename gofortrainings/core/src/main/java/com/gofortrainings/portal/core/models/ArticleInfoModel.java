package com.gofortrainings.portal.core.models;

import java.util.Iterator;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.InjectionStrategy;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.RequestAttribute;
import org.apache.sling.models.annotations.injectorspecific.ResourcePath;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.day.cq.wcm.api.Page;
import com.gofortrainings.portal.core.services.FirstService;

@Model(adaptables=SlingHttpServletRequest.class,defaultInjectionStrategy=DefaultInjectionStrategy.OPTIONAL)
public class ArticleInfoModel {

	@ValueMapValue
	private String subheading;
	
	@ValueMapValue
	private String title;
	
	@ValueMapValue
	private String readmins;
	
	@ValueMapValue
	private String pubdate;
	
	@ValueMapValue
	private String articlepath;
	
	@ValueMapValue(injectionStrategy=InjectionStrategy.REQUIRED,name="sling:resourceType")
	private String articleimage;
	
	@OSGiService
	private FirstService firstService;
	
	@SlingObject
	private SlingHttpServletRequest request;
	
	@ScriptVariable
	private Page currentPage;
	
	@ResourcePath(path="/conf/gofortrainings/settings/wcm/templates/home-page/structure/jcr:content/root/navigation/navdata")
	private Resource myResource;
	
	@RequestAttribute
	private String trainingType;
	
	private String data;
	
	private String searchTerm;
	
	private String pageTitle;
	
	@PostConstruct
	public void init() {
			
		
		if(articlepath!=null && articlepath.startsWith("/content/gofortrainings")) {
			articlepath = articlepath+".html";
		}
		
		data = firstService.getData();
		searchTerm = request.getParameter("q");
		
		pageTitle = currentPage.getTitle();
		
		if(trainingType.equals("live")) {
			pageTitle ="Live AEm Training";
		}
		
		if(trainingType.equals("classroom")) {
			pageTitle ="Classroom AEm Training";
		}
		
		Iterator<Resource> childNodes =myResource.listChildren();
		while (childNodes.hasNext()) {
			Resource resource = (Resource) childNodes.next();
			
		}
	}
	
	
	
	public String getTrainingType() {
		return trainingType;
	}



	public String getSearchTerm() {
		return searchTerm;
	}




	public String getPageTitle() {
		return pageTitle;
	}




	public String getData() {
		return data;
	}



	public String getSubheading() {
		return subheading;
	}

	public String getTitle() {
		return title;
	}

	public String getReadmins() {
		return readmins;
	}

	public String getPubdate() {
		return pubdate;
	}

	public String getArticlepath() {
		return articlepath;
	}

	public String getArticleimage() {
		return articleimage;
	}
	
	
}
