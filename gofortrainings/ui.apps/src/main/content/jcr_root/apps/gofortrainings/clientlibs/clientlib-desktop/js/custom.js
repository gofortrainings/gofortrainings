$(document).ready(function(){

    $.ajax({
        url: "/content/resourcedata.articles.json",
        success: function(data,status){
            var result = data.result;
            for(var obj in result) {
                var pageobj = result[obj];
                
                var html = "<div class='col-md-4 d-flex ftco-animate fadeInUp ftco-animated'>"+
                            "	<div class='blog-entry justify-content-end'>"+
                            "	  <a href='blog-single.html' class='block-20' style='background-image: url(\""+pageobj.thumbnail+"\");'>"+
                            "	  </a>"+
                            "	  <div class='text p-4 float-right d-block'>"+
                            "		<div class='topper d-flex align-items-center'>"+
                            "			<div class='one py-2 pl-3 pr-1 align-self-stretch'>"+
                            "				<span class='day'>18</span>"+
                            "			</div>"+
                            "			<div class='two pl-0 pr-3 py-2 align-self-stretch'>"+
                            "				<span class='yr'>2019</span>"+
                            "				<span class='mos'>October</span>"+
                            "			</div>"+
                            "		</div>"+
                            "		<h3 class='heading mb-3'><a href='#'>"+pageobj.title+"</a></h3>"+
                            "		<p>"+pageobj.description+"</p>"+
                            "		<p><a href='"+pageobj.path+"' class='btn-custom'><span class='ion-ios-arrow-round-forward mr-3'></span>Read more</a></p>"+
                            "	  </div>"+
                            "	</div>"
                            
                $("#article-grid").append(html);
            }
        },
        error: function(data){
            console.log("error");
			debugger;
        }
    });

});